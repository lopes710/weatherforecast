//
//  URLSessionNetworkAccess.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "URLSessionNetworkAccess.h"

@implementation URLSessionNetworkAccess

- (void)getRequestWithURL:(NSString *)URL
               parameters:(NSDictionary *)parameters
                  success:(SuccessBlock)success
                  failure:(FailureBlock)failure {
    
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithString:URL];
    NSMutableArray *queryItems = [NSMutableArray array];
    for (NSString *key in parameters) {
        
        NSURLQueryItem *queryItem = [[NSURLQueryItem alloc] initWithName:key
                                                                   value:parameters[key]];
        [queryItems addObject:queryItem];
    }
    urlComponents.queryItems = queryItems;
    NSURL *weatherURL = urlComponents.URL;
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *sessionDataTask = [defaultSession dataTaskWithURL:weatherURL
                                                          completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                              
                                                              if (error) {
                                                                  failure(error);
                                                              }
                                                              
                                                              if (data) {
                                                                  NSError *errorSerialize;
                                                                  NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                               options:0
                                                                                                                                 error:&errorSerialize];
                                                                  if (errorSerialize) {
                                                                      failure(errorSerialize);
                                                                  }
                                                                  
                                                                  success(responseDict);
                                                              }
                                                          }];
    [sessionDataTask resume];
}

@end
