//
//  TableViewModel.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class TableViewModel;

@interface TableViewModel : NSObject

@property (nonatomic, copy, readonly) NSMutableArray<NSString *> *cities;

- (instancetype)initWithTableView:(UITableView *)tableView;

- (void)addCityName:(NSString *)cityName;

- (void)setEditMode:(BOOL)editMode;

@end
