//
//  CityWeatherViewModelController.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "CityWeatherViewModelController.h"
#import "WeatherAPI.h"
#import "URLSessionNetworkAccess.h"
#import "WeatherTableViewCell.h"
#import "Weather.h"

@interface CityWeatherViewModelController () <UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WeatherAPI *weatherAPI;
@property (nonatomic, strong) CityWeatherViewModel *cityWeatherViewModel;

@end

@implementation CityWeatherViewModelController

- (instancetype)initWithTableView:(UITableView *)tableView {
    
    self = [super init];
    if (self) {
        _tableView = tableView;
        _tableView.dataSource = self;
        _tableView.alwaysBounceVertical = NO;
        
        URLSessionNetworkAccess *urlSessionNetworkAccess = [[URLSessionNetworkAccess alloc] init];
        _weatherAPI = [[WeatherAPI alloc] initWithNetworkAccess:urlSessionNetworkAccess];
    }
    return self;
}

#pragma mark - Public methods

- (void)loadForecastForCityName:(NSString *)cityName
                        success:(SuccessForecastBlock)success
                        failure:(FailureForecastBlock)failure {
    __weak typeof(self) weakSelf = self;
    [self.weatherAPI getCityWeatherWithName:cityName
                                    success:^(id  _Nullable responseObject) {
                                        __strong typeof(self) strongSelf = weakSelf;
                                        
                                        if (strongSelf) {
                                            strongSelf.cityWeatherViewModel = (CityWeatherViewModel *)responseObject;
                                            success(strongSelf.cityWeatherViewModel);
                                        } else {
                                            failure([NSError errorWithDomain:NSCocoaErrorDomain
                                                                        code:0
                                                                    userInfo:@{}]);
                                        }
                                    } failure:^(NSError * _Nullable error) {
                                        failure(error);
                                    }];
}

- (void)reloadData {
    [self.tableView reloadData];
}

#pragma mark - UItableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.cityWeatherViewModel getWeatherDays].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WeatherTableViewCell *weatherCell = [tableView dequeueReusableCellWithIdentifier:[WeatherTableViewCell reuseIdentifier]
                                                                 forIndexPath:indexPath];
    Weather *weather = [self.cityWeatherViewModel getWeatherDays][indexPath.row];
    [weatherCell configureCellWithDate:weather.date
                              maxTempC:weather.maxTempC
                              minTempC:weather.minTempC];
    return weatherCell;
}

@end
