# README #

### What is this repository for? ###

* To get current and forecast weather of a city for the next 5 days.

### Review ###

* This project was architectured following the MVVM design. Other design patterns include a facade 
pattern for the weatherAPI, categories for safe access of data when parsing and protocol design for 
abstraction of the network layer (which also facilitates mock data for requests tests). 
The UI was built using a storyboard.

* CGD was used for background http requests and parsing. Memory management was closely attended.

* TDD was followed for the weatherAPI and WeatherParser, the ones I consider more prone for bugs because
of the data management.

### TODOs ###

* More unit tests could of course be written to increase % coverage.

### Who do I talk to? ###

* duarte.lopes85@gmail.com
