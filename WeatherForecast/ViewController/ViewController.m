//
//  ViewController.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "ViewController.h"
#import "TableViewModel.h"
#import "CityWeatherViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UITextField *cityTextField;
@property (nonatomic, weak) IBOutlet UIButton *addCityButton;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) TableViewModel *tableViewModel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableViewModel = [[TableViewModel alloc] initWithTableView:self.tableView];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Actions

- (IBAction)AddButtonPressed:(UIButton *)sender {
    [self.tableViewModel addCityName:self.cityTextField.text];
}

- (void)setEditing:(BOOL)editing
          animated:(BOOL)animated {
    
    [super setEditing:editing
             animated:animated];
    [self.tableViewModel setEditMode:editing];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCityWeather"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *cityName = self.tableViewModel.cities[indexPath.row];
        
        CityWeatherViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.cityName = cityName;
    }
}

@end
