//
//  NetworkAccessProtocol.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessBlock)(id _Nullable responseObject);
typedef void(^FailureBlock)(NSError * _Nullable error);

@protocol NetworkAccessProtocol <NSObject>

- (void)getRequestWithURL:(nullable NSString *)URL
               parameters:(nullable NSDictionary *)parameters
                  success:(nullable SuccessBlock)success
                  failure:(nullable FailureBlock)failure;

@end
