//
//  CityWeather.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Weather;

@interface CityWeather : NSObject

@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *currentWeatherDescription;
@property (nonatomic, strong, readonly) NSString *currentWeatherIconURL;
@property (nonatomic, copy, readonly) NSArray<Weather *> *weatherDays;

- (instancetype)initWithTitle:(NSString *)title
           currentWeatherDesc:(NSString *)currentWeatherDesc
        currentWeatherIconUrl:(NSString *)currentWeatherIconUrl
                  weatherDays:(NSArray *)weatherDays;

@end
