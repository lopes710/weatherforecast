//
//  SuccessNetworkAccess.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "SuccessNetworkAccess.h"

@implementation SuccessNetworkAccess

- (void)getRequestWithURL:(NSString *)URL
               parameters:(NSDictionary *)parameters
                  success:(SuccessBlock)success
                  failure:(FailureBlock)failure {
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filePath = [bundle pathForResource:@"cityWeather"
                                          ofType:@"json"];
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    NSError *error = nil;
    NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:0
                                                              error:&error];
    
    success(payload);
}

@end
