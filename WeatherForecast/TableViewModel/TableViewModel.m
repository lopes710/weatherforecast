//
//  TableViewModel.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "TableViewModel.h"

static NSString * const cellIdentifier = @"StantardCell";

@interface TableViewModel () <UITableViewDelegate ,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSMutableArray<NSString *> *cities;

@end

@implementation TableViewModel

- (instancetype)initWithTableView:(UITableView *)tableView {

    self = [super init];
    if (self) {
        _tableView = tableView;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // TODO: Just initial set example of cities, this could be removed
        _cities = [[NSMutableArray alloc] initWithArray:@[
                                                         @"Valladolid",
                                                         @"Dublin",
                                                         @"Barcelona",
                                                         @"New York",
                                                         @"Porto"
                                                         ]];
    }
    return self;
}

#pragma mark - Public methods

- (void)addCityName:(NSString *)cityName {
    [self.cities addObject:cityName];
    [self.tableView reloadData];
}

- (void)setEditMode:(BOOL)editMode {

    [self.tableView setEditing:editMode
                      animated:YES];
}

#pragma mark - UItableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {

    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                 forIndexPath:indexPath];
    cell.textLabel.text = self.cities[indexPath.row];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [self.cities removeObjectAtIndex:indexPath.row];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationLeft];
        [tableView endUpdates];
    }
}

@end
