//
//  WeatherAPI.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "WeatherAPI.h"
#import "WeatherParser.h"
#import "CityWeatherViewModel.h"

static NSString * const WeatherURL = @"http://api.worldweatheronline.com/premium/v1/weather.ashx";
static NSString * const APIKey = @"ba8b3d64f220495a942153329172802";
static NSString * const NumberOfDays = @"5";

@interface WeatherAPI ()

@property (nonatomic, strong) id<NetworkAccessProtocol> networkAccess;

@end

@implementation WeatherAPI

- (instancetype)initWithNetworkAccess:(id<NetworkAccessProtocol>)networkAccess {
    
    self = [super init];
    if (self) {
        _networkAccess = networkAccess;
    }
    return self;
}

- (void)getCityWeatherWithName:(NSString *)cityName
                       success:(SuccessBlock)success
                       failure:(FailureBlock)failure {
    
    NSDictionary *parameters = @{
                                 @"q": cityName,
                                 @"format": @"json",
                                 @"num_of_days": NumberOfDays,
                                 @"key": APIKey
                                 };
    
    [self.networkAccess getRequestWithURL:WeatherURL
                               parameters:parameters
                                  success:^(id  _Nullable responseObject) {
                                      
                                      CityWeatherViewModel *cityWeatherViewModel = [WeatherParser parseWithPayload:responseObject];
                                      success(cityWeatherViewModel);
                                      
                                  } failure:^(NSError * _Nullable error) {
                                      failure(error);
                                  }];
}

@end
