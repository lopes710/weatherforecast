//
//  URLSessionNetworkAccess.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkAccessProtocol.h"

@interface URLSessionNetworkAccess : NSObject <NetworkAccessProtocol>

@end
