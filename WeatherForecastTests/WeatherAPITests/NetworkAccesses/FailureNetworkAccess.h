//
//  FailureNetworkAccess.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkAccessProtocol.h"

@interface FailureNetworkAccess : NSObject <NetworkAccessProtocol>

@end
