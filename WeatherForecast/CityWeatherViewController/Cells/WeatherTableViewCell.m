//
//  WeatherTableViewCell.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "WeatherTableViewCell.h"

@interface WeatherTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *maxTempCLabel;
@property (nonatomic, weak) IBOutlet UILabel *minTempCLabel;

@end

@implementation WeatherTableViewCell

+ (NSString *)reuseIdentifier {

    return NSStringFromClass([self class]);
}

- (void)configureCellWithDate:(NSString *)date
                     maxTempC:(NSString *)maxTempC
                     minTempC:(NSString *)minTempC {

    self.dateLabel.text = date;
    self.minTempCLabel.text = minTempC;
    self.maxTempCLabel.text = maxTempC;
}

@end
