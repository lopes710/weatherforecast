//
//  CityWeatherViewController.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "CityWeatherViewController.h"
#import "CityWeatherViewModelController.h"

@interface CityWeatherViewController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UITableView *forecastTableView;
@property (nonatomic, strong) CityWeatherViewModelController *cityWeatherViewModelController;

@end

@implementation CityWeatherViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cityWeatherViewModelController = [[CityWeatherViewModelController alloc] initWithTableView:self.forecastTableView];
    __weak typeof(self) weakSelf = self;
    [self.cityWeatherViewModelController loadForecastForCityName:self.cityName
                                                         success:^(CityWeatherViewModel *cityWeatherViewModel) {
                                                             __strong typeof(self) strongSelf = weakSelf;
                                                             
                                                             if (strongSelf) {
                                                                 [strongSelf updateUI:cityWeatherViewModel];
                                                                 [strongSelf updateImageIcon:cityWeatherViewModel];
                                                             }
                                                         } failure:^(NSError *error) {
                                                             NSLog(@"Error: %@", error.localizedDescription);
                                                         }];
}

#pragma mark - Private methods

- (void)updateUI:(CityWeatherViewModel *)cityWeatherViewModel {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titleLabel.text = [cityWeatherViewModel getTitle];
        self.descriptionLabel.text = [cityWeatherViewModel getCurrentWeatherDescription];
        [self.cityWeatherViewModelController reloadData];
    });
}

- (void)updateImageIcon:(CityWeatherViewModel *)cityWeatherViewModel {
    __weak typeof(self) weakSelf = self;
    [cityWeatherViewModel getCurrentWeatherIconSuccess:^(UIImage * _Nullable image) {
        __strong typeof(self) strongSelf = weakSelf;
        
        if (strongSelf) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageView.image = image;
            });
        }
    } failure:^(NSError * _Nullable error) {
        NSLog(@"Error loading image: %@", error.localizedDescription);
    }];
}

@end
