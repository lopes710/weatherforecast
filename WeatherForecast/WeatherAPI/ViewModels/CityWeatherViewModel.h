//
//  CityWeatherViewModel.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class CityWeather;

typedef void(^SuccessImageBlock)(UIImage * _Nullable image);
typedef void(^FailureimageBlock)(NSError * _Nullable error);

@interface CityWeatherViewModel : NSObject

- (instancetype)initWithCityWeather:(CityWeather *)cityWeather;

- (nonnull NSString *)getTitle;
- (nonnull NSString *)getCurrentWeatherDescription;
- (void)getCurrentWeatherIconSuccess:(SuccessImageBlock)successImage
                             failure:(FailureimageBlock)failureImage;
- (nonnull NSArray *)getWeatherDays;

@end
