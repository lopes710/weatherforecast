//
//  CityWeatherViewModel.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "CityWeatherViewModel.h"
#import "CityWeather.h"

@interface CityWeatherViewModel ()

@property (nonatomic, strong) CityWeather *cityWeather;

@end

@implementation CityWeatherViewModel

#pragma mark - Public methods

- (instancetype)initWithCityWeather:(CityWeather *)cityWeather {

    self = [super init];
    if (self) {
        _cityWeather = cityWeather;
    }
    return self;
}

- (NSString *)getTitle {
    return self.cityWeather.title;
}

- (nonnull NSString *)getCurrentWeatherDescription {
    return self.cityWeather.currentWeatherDescription;
}

- (void)getCurrentWeatherIconSuccess:(SuccessImageBlock)successImage
                             failure:(FailureimageBlock)failureImage {
    NSURL *imageUrl = [NSURL URLWithString:self.cityWeather.currentWeatherIconURL];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:imageUrl
                                                         completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                             if (data) {
                                                                 UIImage *image = [UIImage imageWithData:data];
                                                                 successImage(image);
                                                             } else {
                                                                 failureImage(error);
                                                             }
                                                         }];
    [task resume];
}

- (NSArray *)getWeatherDays {
    return self.cityWeather.weatherDays;
}
@end
