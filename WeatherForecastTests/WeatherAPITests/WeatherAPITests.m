//
//  WeatherAPITests.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WeatherAPI.h"
#import "EmptyNetworkAccess.h"
#import "FailureNetworkAccess.h"
#import "SuccessNetworkAccess.h"
#import "CityWeatherViewModel.h"

@interface WeatherAPITests : XCTestCase

@end

@implementation WeatherAPITests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testSuccessEmptyCityWeather {
    
    EmptyNetworkAccess *emptyAccessNetwork = [[EmptyNetworkAccess alloc] init];
    WeatherAPI *weatherAPI = [[WeatherAPI alloc] initWithNetworkAccess:emptyAccessNetwork];
    
    [weatherAPI getCityWeatherWithName:@"Valladolid"
                               success:^(id  _Nullable responseObject) {
                                   CityWeatherViewModel *cityWeatherViewModel = (CityWeatherViewModel *)responseObject;
                                   XCTAssertNotNil(cityWeatherViewModel);
                               } failure:^(NSError * _Nullable error) {
                                   
                               }];
}

- (void)testFailureCityWeather {
    
    FailureNetworkAccess *failureAccessNetwork = [[FailureNetworkAccess alloc] init];
    WeatherAPI *weatherAPI = [[WeatherAPI alloc] initWithNetworkAccess:failureAccessNetwork];
    
    [weatherAPI getCityWeatherWithName:@"Valladolid"
                               success:^(id  _Nullable responseObject) {
                               } failure:^(NSError * _Nullable error) {
                                   XCTAssertNotNil(error);
                               }];
}

- (void)testSuccessCityWeather {
    
    SuccessNetworkAccess *successAccessNetwork = [[SuccessNetworkAccess alloc] init];
    WeatherAPI *weatherAPI = [[WeatherAPI alloc] initWithNetworkAccess:successAccessNetwork];
    
    [weatherAPI getCityWeatherWithName:@"Valladolid"
                               success:^(id  _Nullable responseObject) {
                                   CityWeatherViewModel *cityWeatherViewModel = (CityWeatherViewModel *)responseObject;
                                   XCTAssertNotNil(cityWeatherViewModel);
                                   XCTAssertEqualObjects([cityWeatherViewModel getTitle], @"Valladolid, Spain");
                                   XCTAssertEqualObjects([cityWeatherViewModel getCurrentWeatherDescription], @"Light Drizzle");
                                   XCTAssertEqual([cityWeatherViewModel getWeatherDays].count, 5);
                               } failure:^(NSError * _Nullable error) {
                                   
                               }];
}

@end
