//
//  WeatherParser.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "WeatherParser.h"
#import "NSDictionary+SafeAccess.h"
#import "CityWeather.h"
#import "Weather.h"
#import "CityWeatherViewModel.h"

static NSString * const DataKey = @"data";
static NSString * const RequestKey = @"request";
static NSString * const QueryKey = @"query";
static NSString * const CurrentConditionKey = @"current_condition";
static NSString * const WeatherDescKey = @"weatherDesc";
static NSString * const WeatherIconUrlKey = @"weatherIconUrl";
static NSString * const ValueKey = @"value";
static NSString * const WeatherKey = @"weather";
static NSString * const DateKey = @"date";
static NSString * const MaxTempCKey = @"maxtempC";
static NSString * const MinTempCKey = @"mintempC";

@implementation WeatherParser

+ (CityWeatherViewModel *)parseWithPayload:(NSDictionary *)payload {
    
    NSDictionary *dataDict = [payload valueForKey:DataKey
                                         ifKindOf:[NSDictionary class]
                                     defaultValue:@{}];
    
    NSArray *requestArray = [dataDict valueForKey:RequestKey
                                         ifKindOf:[NSArray class]
                                     defaultValue:@[]];
    
    // Request
    NSDictionary *requestDict = [requestArray firstObject];
    NSString *title = [requestDict valueForKey:QueryKey
                                      ifKindOf:[NSString class]
                                  defaultValue:@""];
    
    // Current Condition
    NSArray *currentConditionArray = [dataDict valueForKey:CurrentConditionKey
                                                  ifKindOf:[NSArray class]
                                              defaultValue:@[]];
    
    NSDictionary *currentConditionDict = [currentConditionArray firstObject];
    
    // Weather Desc
    NSArray *weatherDescArray = [currentConditionDict valueForKey:WeatherDescKey
                                                         ifKindOf:[NSArray class]
                                                     defaultValue:@[]];
    NSDictionary *weatherDescDict = [weatherDescArray firstObject];
    NSString *weatherDesc = [weatherDescDict valueForKey:ValueKey
                                                ifKindOf:[NSString class]
                                            defaultValue:@""];
    
    // Weather Icon Url
    NSArray *weatherIconUrlArray = [currentConditionDict valueForKey:WeatherIconUrlKey
                                                            ifKindOf:[NSArray class]
                                                        defaultValue:@[]];
    NSDictionary *weatherIconUrlDict = [weatherIconUrlArray firstObject];
    NSString *weatherIconUrl = [weatherIconUrlDict valueForKey:ValueKey
                                                      ifKindOf:[NSString class]
                                                  defaultValue:@""];
    
    // Weather
    NSArray *weatherArray = [dataDict valueForKey:WeatherKey
                                         ifKindOf:[NSArray class]
                                     defaultValue:@[]];
    
    NSMutableArray *weatherDays = [[NSMutableArray alloc] init];
    for (NSDictionary *weatherDict in weatherArray) {
        
        NSString *date = [weatherDict valueForKey:DateKey
                                         ifKindOf:[NSString class]
                                     defaultValue:@""];
        
        NSString *maxTempC = [weatherDict valueForKey:MaxTempCKey
                                             ifKindOf:[NSString class]
                                         defaultValue:@""];
        
        NSString *minTempC = [weatherDict valueForKey:MinTempCKey
                                             ifKindOf:[NSString class]
                                         defaultValue:@""];
        
        Weather *weather = [[Weather alloc] initWithDate:date
                                                maxTempC:maxTempC
                                                minTempC:minTempC];
        [weatherDays addObject:weather];
    }
    
    CityWeather *cityWeather = [[CityWeather alloc] initWithTitle:title
                                               currentWeatherDesc:weatherDesc
                                            currentWeatherIconUrl:weatherIconUrl
                                                      weatherDays:weatherDays];
    
    CityWeatherViewModel *cityWeatherViewModel = [[CityWeatherViewModel alloc] initWithCityWeather:cityWeather];
    return cityWeatherViewModel;
}

@end
