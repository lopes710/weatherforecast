//
//  EmptyNetworkAccess.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "EmptyNetworkAccess.h"

@interface EmptyNetworkAccess () 

@end

@implementation EmptyNetworkAccess

- (void)getRequestWithURL:(NSString *)URL
               parameters:(NSDictionary *)parameters
                  success:(SuccessBlock)success
                  failure:(FailureBlock)failure {

    success(@{});
}

@end
