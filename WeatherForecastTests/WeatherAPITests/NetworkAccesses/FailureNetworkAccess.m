//
//  FailureNetworkAccess.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "FailureNetworkAccess.h"

@implementation FailureNetworkAccess

- (void)getRequestWithURL:(NSString *)URL
               parameters:(NSDictionary *)parameters
                  success:(SuccessBlock)success
                  failure:(FailureBlock)failure {
    
    NSError *error = [NSError errorWithDomain:NSURLErrorDomain
                                         code:404
                                     userInfo:@{
                                                @"Error": @"Something happened"
                                                }];
    failure(error);
}

@end
