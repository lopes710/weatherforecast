//
//  Weather.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (nonatomic, strong, readonly) NSString *date;
@property (nonatomic, strong, readonly) NSString *maxTempC;
@property (nonatomic, strong, readonly) NSString *minTempC;

- (instancetype)initWithDate:(NSString *)date
                    maxTempC:(NSString *)maxTempC
                    minTempC:(NSString *)minTempC;

@end
