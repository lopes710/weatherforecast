//
//  WeatherParser.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "CityWeatherViewModel.h"

@interface WeatherParser : NSObject

+ (CityWeatherViewModel *)parseWithPayload:(NSDictionary *)payload;

@end
