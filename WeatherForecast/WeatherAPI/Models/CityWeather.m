//
//  CityWeather.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "CityWeather.h"

@interface CityWeather ()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *currentWeatherDescription;
@property (nonatomic, strong) NSString *currentWeatherIconURL;
@property (nonatomic, copy) NSArray<Weather *> *weatherDays;

@end

@implementation CityWeather

- (instancetype)initWithTitle:(NSString *)title
           currentWeatherDesc:(NSString *)currentWeatherDesc
        currentWeatherIconUrl:(NSString *)currentWeatherIconUrl
                  weatherDays:(NSArray *)weatherDays {

    self = [super init];
    if (self) {
        _title = title;
        _currentWeatherDescription = currentWeatherDesc;
        _currentWeatherIconURL = currentWeatherIconUrl;
        _weatherDays = weatherDays;
    }
    return self;
}

@end
