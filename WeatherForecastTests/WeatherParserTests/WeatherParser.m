//
//  WeatherParserTests.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CityWeatherViewModel.h"
#import "WeatherParser.h"

@interface WeatherParserTests : XCTestCase

@property (nonatomic, copy) NSDictionary *payload;

@end

@implementation WeatherParserTests

- (void)setUp {
    [super setUp];
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filePath = [bundle pathForResource:@"cityWeather"
                                          ofType:@"json"];
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    NSError *error = nil;
    self.payload = [NSJSONSerialization JSONObjectWithData:jsonData
                                                   options:0
                                                     error:&error];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testWeatherParserWithEmptyPayload {
    
    CityWeatherViewModel *cityWeatherViewModel = [WeatherParser parseWithPayload:@{}];
    XCTAssertNotNil(cityWeatherViewModel);
}

- (void)testWeatherParserWithPayload {
    
    CityWeatherViewModel *cityWeatherViewModel = [WeatherParser parseWithPayload:self.payload];
    
    XCTAssertNotNil(cityWeatherViewModel);
    XCTAssertEqualObjects([cityWeatherViewModel getTitle], @"Valladolid, Spain");
    XCTAssertEqualObjects([cityWeatherViewModel getCurrentWeatherDescription], @"Light Drizzle");
    XCTAssertEqual([cityWeatherViewModel getWeatherDays].count, 5);
}

@end
