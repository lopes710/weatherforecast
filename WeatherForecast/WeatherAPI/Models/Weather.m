//
//  Weather.m
//  WeatherForecast
//
//  Created by Duarte Lopes on 28/02/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import "Weather.h"

@interface Weather ()

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *maxTempC;
@property (nonatomic, strong) NSString *minTempC;

@end

@implementation Weather

- (instancetype)initWithDate:(NSString *)date
                    maxTempC:(NSString *)maxTempC
                    minTempC:(NSString *)minTempC {
    
    self = [super init];
    if (self) {
        _date = date;
        _maxTempC = maxTempC;
        _minTempC = minTempC;
    }
    return self;
}

@end
