//
//  CityWeatherViewController.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityWeatherViewController : UIViewController

@property (nonatomic, copy) NSString *cityName;

@end
