//
//  WeatherAPI.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkAccessProtocol.h"

@interface WeatherAPI : NSObject

- (instancetype)initWithNetworkAccess:(id<NetworkAccessProtocol>)networkAccess;

- (void)getCityWeatherWithName:(NSString *)cityName
                       success:(SuccessBlock)success
                       failure:(FailureBlock)failure;

@end
