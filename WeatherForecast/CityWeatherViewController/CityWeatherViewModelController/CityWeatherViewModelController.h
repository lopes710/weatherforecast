//
//  CityWeatherViewModelController.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CityWeatherViewModel.h"

typedef void(^SuccessForecastBlock)(CityWeatherViewModel *cityWeatherViewModel);
typedef void(^FailureForecastBlock)(NSError *error);

@interface CityWeatherViewModelController : NSObject

- (instancetype)initWithTableView:(UITableView *)tableView;

- (void)loadForecastForCityName:(NSString *)cityName
                        success:(SuccessForecastBlock)success
                        failure:(FailureForecastBlock)failure;

- (void)reloadData;

@end
