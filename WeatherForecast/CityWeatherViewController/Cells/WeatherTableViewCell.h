//
//  WeatherTableViewCell.h
//  WeatherForecast
//
//  Created by Duarte Lopes on 01/03/2017.
//  Copyright © 2017 Duarte Lopes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherTableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;

- (void)configureCellWithDate:(NSString *)date
                     maxTempC:(NSString *)maxTempC
                     minTempC:(NSString *)minTempC;

@end
